A CMake project that builds the Arduino SDK for ArduinoDue. It exports a
`ArduinoDue::` namespace featuring the `ArduinoDue::core` target, with the core
SDK, and the additional `ArduinoDue::SPI, ArduinoDue::Wire, ArduinoDue::HID`

## Build the Core Arduino SDK
1. Create a build directory
2. Go to the build directory and run

```
cmake -DCMAKE_TOOLCHAIN_FILE=$(realpath ../cmake/Arduino-Toolchain.cmake)
-DCMAKE_INSTALL_PREFIX=<install_path> ..
```

## Build a sketch using the installed SDK
In order to use the compiled SDK and build your sketch, you can check the
[examples][./examples], where you'll find also examples that require third
party libraries. This CMake project exports the `add_sketch` function to define
the main sketch. The `add_sketch` originates a `sketch` target. All the
libraries it needs need to be linked using `target_link_libraries`. 

```
target_link_libraries(sketch <additiona_lib_target>)
```

You also need to link `SPI, Wire or HID` if needed.

To build your project you then run (replace `<install_path>` with the install
path you used before to build the SDK)
```
cmake -DCMAKE_TOOLCHAIN_FILE="<install_path>/lib/cmake/Arduino-Toolchain.cmake"
-DArduinoDue_DIR="<install_path>/lib/cmake/ArduinoDueSDK" ..
```
