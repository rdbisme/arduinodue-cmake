/******************************************************************************
  COMBUSTION CHAMBER TEMPERATURE DATA ACQUISITION
  Test: temp sensor, 0-800°C, type K.

  Code derived from the Adafruit tutorial for the MAX31856 module:
  https://learn.adafruit.com/adafruit-max31856-thermocouple-amplifier/wiring-and-test
******************************************************************************/


// LIBRARIES

#include <Adafruit_MAX31856.h>


// WIRE CONNECTIONS

// Digital pins for the Adafruit MAX31856 module.
// Use software SPI: CS, DI, DO, CLK. Any digital pin can be utilised.
Adafruit_MAX31856 max = Adafruit_MAX31856(10, 11, 12, 13);


// INTERNAL PARAMETERS

// Temperature value of the cold junction (inside the module) [°C].
float temp_cold_junct = 0;
// Temperature value [°C]. The Adafruit MAX31856 temp out can be "float" type.
float temp_value = 0;


void setup()
{
  // Setup Serial Monitor: communication with the PC in order to print the pressure
  // value on the screen.
  Serial.begin(9600); 

  // Initialize the module.
  max.begin();

  // Set the thermocouple type (type K for this test):
  // max.setThermocoupleType(MAX31856_TCTYPE_xxx) 
  // where "xxx" = B, E, J, K, N, R, S, T, G8, G32.
  max.setThermocoupleType(MAX31856_TCTYPE_K);
}


void loop()
{
    // Read temperature of the junction inside the module [°C].
    temp_cold_junct = max.readCJTemperature();
    
    // Read the temperature value [°C].
    temp_value = max.readThermocoupleTemperature();

    // Print the pressure value to the PC screen:
    // Print the temp value of the cold junction (inside the module).
    // Print the text "Temp cold junction: ";
    Serial.print("Temperature cold junction: ");
    // Print the temperature value [°C].
    Serial.print(temp_cold_junct);
    // Print the text "  °C  Temperature Value: " (unit for the previous value).
    Serial.print("  °C  Temperature Value: ");
    // Print the temperature value [°C].
    Serial.print(temp_value);
    // Print the unit and create a new line for the next loop.
    Serial.println(" °C");
    
    // Add a delay [ms] between each loop, to reduce the number of printed lines.
    delay(500);
}
