#######################################################################
# 
#     List subdirectories of a directory and put the list into a variable
#     
#     params[in]  DIR    the directory to search
#     params[out] VAR    the variable name  
# 
#######################################################################
function(get_subdirs DIR VAR)
    set(options RECURSIVE)
    set(oneValueArgs "")
    set(multiValueArgs "")
    cmake_parse_arguments(PARSE_ARGV 2 SUBDIRS 
        "${options}"
        "${oneValueArgs}" 
        "${multiValueArgs}")

    if(SUBDIRS_RECURSIVE)
        file(GLOB_RECURSE children 
            LIST_DIRECTORIES TRUE 
            RELATIVE ${DIR}
            ${DIR}/*)
    else()
        file(GLOB children RELATIVE ${DIR} ${DIR}/*)
    endif()

    foreach(child ${children})
        if(IS_DIRECTORY "${DIR}/${child}")
            list(APPEND dirlist ${child})
        endif()
    endforeach()


    set(${VAR} ${dirlist} PARENT_SCOPE)

endfunction()

#######################################################################
#
#     Creates the executable targer for the SDK's <main.cpp>
# 
#######################################################################
function(_add_main_executable)
    add_executable(main ${TOOLCHAIN_SDK_CORE_MAIN_FILE})
    target_link_options(main PRIVATE ${TOOLCHAIN_LINKER_FLAGS})
    target_link_libraries(main PUBLIC ArduinoDue::core)

    # Add custom target for the OBJCOPY step
    add_custom_command(TARGET main POST_BUILD
        COMMAND ${CMAKE_OBJCOPY} -O binary main main.bin
        COMMAND 
        COMMENT "Generate raw binary"
    )

endfunction()


##############################################################################
# 
#     Wraps the target_link_libraries in order to correctly create "Core"
#     libraries targets before adding them as target link if they're specified
#     as dependencies for the target
#     
#     params[in] target   The target which needs to be linked against
#     params[in] libs     A list of libraries to link.
#
#     Usage:
#
#     sketch_link_libraries(myTarget myLib SPI thermocouple [...])
#       
#
##############################################################################
function(sketch_link_libraries target)
    # Accept variable number of source files
    set(libs ${ARGN})

    # Add "Core" libraries target
    _get_core_libraries()
    foreach(lib ${libs})
        if(${lib} IN_LIST TOOLCHAIN_SDK_LIBRARIES)
            if(NOT TARGET ${lib})
                _add_core_library(${lib})
            endif()
        endif()
        target_link_libraries(${target} PUBLIC ${lib})
    endforeach()
endfunction()


##############################################################################
#    Wraps the add_executable to link the firmware code with the actual main
#    code and all the libraries of the toolchain SDK. This creates the `sketch`
#    target
#
#   :: Warning :: 
#   This function links the executable *ONLY* agaist the SDK core target. All
#   the external libraries (also the "Core" libraries like SPI or Wire) must be
#   linked using `sketch_link_libraries` manually
# 
#    
#   Usage: 
#   add_sketch(mysketch.ino)
#   
#   sketch_link_libraries(PRIVATE SPI)
#
#
#
##############################################################################
function(add_sketch)
    # Accept variable number of source files
    set(srcs ${ARGV})

    set_source_files_properties(${srcs} PROPERTIES LANGUAGE CXX)
    add_library(sketch OBJECT ${srcs})
    target_link_libraries(sketch PUBLIC ArduinoDue::core)
    target_compile_options(sketch PRIVATE 
        "-xc++"
        -include Arduino.h)
    _add_main_executable()

endfunction()
#######################################################################

##############################################################################
#    Wraps the add_library to link the core firmware code
#
#   :: Warning :: 
#   This function links the executable *ONLY* agaist the SDK core target. All
#   the external libraries (also the "Core" libraries like SPI or Wire) must be
#   linked using `sketch_link_libraries` manually
# 
#   params[in] lib      The name of the library to add
#   params[in] srcs     The sources of the library
#    
#   Usage: 
#   add_sketch_library(
#   
#   sketch_link_libraries(PRIVATE SPI)
#
#
#
##############################################################################
function(add_sketch_library lib)
    # Accept variable number of source files
    set(srcs ${ARGN})

    _add_core_sdk()
    
    add_library(${lib} STATIC ${srcs})
    target_link_libraries(${lib} PUBLIC core)
endfunction()
#######################################################################
